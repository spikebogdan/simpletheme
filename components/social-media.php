<?php global $adrc_theme; ?>
<ul class="social-media">
	<li class="social"><a target="_blank" href="<?= $adrc_theme['facebook']; ?>"><i class="fa fa-facebook"></i></a></li>
	<li class="social"><a target="_blank" href="<?= $adrc_theme['youtube']; ?>"><i class="fa fa-youtube"></i></a></li>
	<li class="social"><a target="_blank" href="<?= $adrc_theme['twitter']; ?>"><i class="fa fa-twitter"></i></a></li>	
	<li class="social"><a target="_blank" href="<?= $adrc_theme['google']; ?>"><i class="fa fa-gplus"></i></a></li>	
	<li class="social"><a target="_blank" href="<?= $adrc_theme['linkedin']; ?>"><i class="fa fa-linkedin"></i></a></li>	
</ul>
