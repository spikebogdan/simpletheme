<?php 
/**
 * @name Simple Page Template
 * @package simpleTheme
 * 
 */

?>

<?php get_header(); ?>

	<div class="row">
		
	<main id="main col-sm-8">
	</main>
	<!-- main -->

	<aside class="col-sm-4">
		<?php get_sidebar(); ?>
	</aside>

	</div>
	<!-- .row -->

<?php get_footer(); ?>
